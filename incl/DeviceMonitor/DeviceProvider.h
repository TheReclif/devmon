#ifndef DEVMON_DEVICEPROVIDER_INCL
#define DEVMON_DEVICEPROVIDER_INCL

#include "Types.h"

namespace devmon
{
	class DEVICEMONITOR_EXPORT DeviceProvider
	{
	public:
		DeviceProvider() = default;
		virtual ~DeviceProvider() = default;

		DeviceProvider(const DeviceProvider&) = delete;
		DeviceProvider(DeviceProvider&&) noexcept = default;

		DeviceProvider& operator=(const DeviceProvider&) = delete;
		DeviceProvider& operator=(DeviceProvider&&) noexcept = default;

		virtual Properties getProperties() = 0;
	};

	class DEVICEMONITOR_EXPORT JSONFileDeviceProvider
		: public DeviceProvider
	{
	private:
		std::string deviceFilename;
	public:
		JSONFileDeviceProvider() = delete;
		JSONFileDeviceProvider(std::string_view filename);
		virtual ~JSONFileDeviceProvider() = default;

		JSONFileDeviceProvider(const JSONFileDeviceProvider&) = delete;
		JSONFileDeviceProvider(JSONFileDeviceProvider&&) noexcept = default;

		JSONFileDeviceProvider& operator=(const JSONFileDeviceProvider&) = delete;
		JSONFileDeviceProvider& operator=(JSONFileDeviceProvider&&) noexcept = default;

		virtual Properties getProperties() override;
	};
}

#endif
