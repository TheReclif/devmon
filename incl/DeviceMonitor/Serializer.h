#ifndef DEVMON_SERIALIZER_INCL
#define DEVMON_SERIALIZER_INCL

#include "Types.h"

#include <string>

namespace devmon
{
	class DEVICEMONITOR_EXPORT ValueSerializer
	{
	public:
		virtual ~ValueSerializer() = default;

		virtual void serializeInt(int val) = 0;
		virtual void serializeFloat(float val) = 0;
		virtual void serializeString(const std::string& val) = 0;
		virtual void serializeBool(bool val) = 0;
	};

	class DEVICEMONITOR_EXPORT Serializer
	{
	public:
		virtual ~Serializer() = default;

		virtual std::string toStr() const = 0;
		virtual void serialize(const Dictionary& dict) = 0;
		virtual void serialize(const Properties& props) = 0;
	};
}

#endif
