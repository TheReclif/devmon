#ifndef DEVMON_DEVICEMONITOR_INCL
#define DEVMON_DEVICEMONITOR_INCL

#include "DeviceProvider.h"

#include <unordered_map>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>

namespace devmon
{
	class DEVICEMONITOR_EXPORT DeviceMonitor
	{
	private:
		const std::chrono::microseconds delay;

		mutable std::condition_variable cv;
		mutable std::mutex mutex;

		std::thread thread;

		std::unordered_map<std::string, std::unique_ptr<devmon::DeviceProvider>> devices;

		devmon::Dictionary deviceStasuses;

		volatile bool shouldStop;
	private:
		void updateStasuses();
		void threadProc();
	public:
		DeviceMonitor(std::chrono::microseconds period);
		~DeviceMonitor();

		DeviceMonitor(const DeviceMonitor&) = delete;
		DeviceMonitor(DeviceMonitor&&) noexcept = delete;

		DeviceMonitor& operator=(const DeviceMonitor&) = delete;
		DeviceMonitor& operator=(DeviceMonitor&&) noexcept = delete;

		void start();
		void stop();
		Dictionary get_statuses() const;

		bool isRunning() const;

		void addDevice(const std::string& deviceId, std::unique_ptr<DeviceProvider>&& provider);
		void removeDevice(const std::string& deviceId);
		std::size_t getDeviceCount() const;
	};
}

#endif
