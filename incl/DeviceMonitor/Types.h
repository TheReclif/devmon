#ifndef DEVMON_TYPES_INCL
#define DEVMON_TYPES_INCL

#include "Value.h"

#include <unordered_map>

namespace devmon
{
	class Value;

	using Properties = std::unordered_map<std::string, std::unique_ptr<Value>>;
	using Dictionary = std::unordered_map<std::string, Properties>;

	namespace internal
	{
		Properties DEVICEMONITOR_EXPORT copy(const Properties& props);
		Dictionary DEVICEMONITOR_EXPORT copy(const Dictionary& dict);
	}
}

#endif
