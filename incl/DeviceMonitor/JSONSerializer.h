#ifndef DEVMON_JSONSERIALIZER_INCL
#define DEVMON_JSONSERIALIZER_INCL

#include "Serializer.h"

#include <memory>

namespace devmon
{
	class DEVICEMONITOR_EXPORT JSONSerializer
		: public Serializer
	{
	private:
		class impl;
	private:
		std::unique_ptr<impl> implPtr;
	public:
		JSONSerializer();
		virtual ~JSONSerializer();

		JSONSerializer(const JSONSerializer&) = delete;
		JSONSerializer(JSONSerializer&&) noexcept = delete;

		JSONSerializer& operator=(const JSONSerializer&) = delete;
		JSONSerializer& operator=(JSONSerializer&&) noexcept = delete;

		virtual std::string toStr() const override;
		virtual void serialize(const Dictionary& dict) override;
		virtual void serialize(const Properties& props) override;
	};
}

#endif
