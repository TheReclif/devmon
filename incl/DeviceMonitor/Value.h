#ifndef DEVMON_VALUE_INCL
#define DEVMON_VALUE_INCL

#include "devmon_export.h"

#include <string>
#include <string_view>
#include <memory>

namespace devmon
{
	class ValueSerializer;

	class DEVICEMONITOR_EXPORT Value
	{
	public:
		Value() = default;
		
		Value(const Value&) = delete;
		Value(Value&&) noexcept = delete;

		Value& operator=(const Value&) = delete;
		Value& operator=(Value&&) noexcept = delete;

		virtual ~Value() = default;

		virtual std::string toStr() const = 0;
		virtual std::unique_ptr<Value> copy() const = 0;
		virtual void serialize(ValueSerializer& serializer) const = 0;
	};

	class DEVICEMONITOR_EXPORT StringValue
		: public Value
	{
	private:
		std::string value;
	public:
		StringValue() = delete;
		StringValue(std::string_view newValue);

		virtual ~StringValue() = default;

		void setValue(std::string_view newValue);
		std::string getValue() const;

		virtual std::string toStr() const override;
		virtual std::unique_ptr<Value> copy() const override;
		virtual void serialize(ValueSerializer& serializer) const override;
	};

	class DEVICEMONITOR_EXPORT IntValue
		: public Value
	{
	private:
		int value;
	public:
		IntValue() = delete;
		IntValue(int newValue);

		virtual ~IntValue() = default;

		void setValue(int newValue);
		int getValue() const;

		virtual std::string toStr() const override;
		virtual std::unique_ptr<Value> copy() const override;
		virtual void serialize(ValueSerializer& serializer) const override;
	};

	class DEVICEMONITOR_EXPORT FloatValue
		: public Value
	{
	private:
		double value;
	public:
		FloatValue() = delete;
		FloatValue(double newValue);

		virtual ~FloatValue() = default;

		void setValue(double newValue);
		double getValue() const;

		virtual std::string toStr() const override;
		virtual std::unique_ptr<Value> copy() const override;
		virtual void serialize(ValueSerializer& serializer) const override;
	};

	class DEVICEMONITOR_EXPORT BoolValue
		: public Value
	{
	private:
		bool value;
	public:
		BoolValue() = delete;
		BoolValue(bool newValue);

		virtual ~BoolValue() = default;

		void setValue(bool newValue);
		bool getValue() const;

		virtual std::string toStr() const override;
		virtual std::unique_ptr<Value> copy() const override;
		virtual void serialize(ValueSerializer& serializer) const override;
	};
}

#endif
