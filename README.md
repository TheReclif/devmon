# Prerequisites
DeviceMonitor requires `nlohmann json` and `gtest` to be available to CMake e.g. by using a package manager and a CMake toolchain file. Additionaly, to build examples, `argumentum` must also be available.
Only `vcpkg` was used to gather requirements for the project. CMake should automatically download all of the requirements if used with `vcpkg`. You can also use the following `vcpkg` command (in order to make it work on other platforms, change the compiler triplet from x64-windows to e.g. x64-linux):
```
vcpkg install gtest:x64-windows nlohmann-json:x64-windows argumentum:x64-windows
```

# Building
With `vcpkg`:
```
mkdir out
cd out
cmake ../ -DCMAKE_TOOLCHAIN_FILE=<path_to_vcpkg/scripts/buildsystems/vcpkg.cmake>
cmake --build .
```
