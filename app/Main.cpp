#include <DeviceMonitor/DeviceMonitor.h>
#include <DeviceMonitor/JSONSerializer.h>
#include <argumentum/argparse.h>
#include <csignal>

using std::chrono::milliseconds;

namespace
{
	volatile std::sig_atomic_t SignalStatus = 0;
}

void signalHandler(int signal)
{
	SignalStatus = signal;
}

int main(int argc, char** argv)
{
	std::signal(SIGINT, signalHandler);

	std::vector<std::string> filesToMonitor;
	unsigned int delay;

	argumentum::argument_parser parser;
	auto params = parser.params();

	parser.config().program(argv[0]).description("Device Monitor example app. Use Ctrl+C to quit (the signal will be intercepted and handled inside the app).");
	
	params
		.add_parameter(filesToMonitor, "N").minargs(1).metavar("<files>").required(true)
		.help("Files to process as if they were devices");
	params
		.add_parameter(delay, "--delay", "-d").nargs(1).absent(1000).metavar("<delay>").required(false)
		.help("Delay between monitor queries in milliseconds (positive integer)");
	params.add_default_help_option();

	auto result = parser.parse_args(argc, argv);
	if (!result)
	{
		return 1;
	}

	std::cout << "Starting with " << filesToMonitor.size() << " devices and a delay of " << delay << "ms." << std::endl;

	const milliseconds finalDelay(delay);
	devmon::DeviceMonitor monitor(finalDelay);

	for (const auto& file : filesToMonitor)
	{
		monitor.addDevice(file, std::make_unique<devmon::JSONFileDeviceProvider>(file));
	}

	monitor.start();

	devmon::JSONSerializer serializer;
	while (SignalStatus == 0)
	{
		std::this_thread::sleep_for(milliseconds(1000));
		serializer.serialize(monitor.get_statuses());
		std::cout << serializer.toStr() << std::endl;
	}

	// No need to call stop() - will be called by the dtor.

	return 0;
}
