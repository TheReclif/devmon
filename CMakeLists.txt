cmake_minimum_required(VERSION 3.16)

project(DeviceMonitor LANGUAGES CXX)

include(GenerateExportHeader)

option(COMPILE_TESTS "Compile the test suite" ON)
option(COMPILE_EXAMPLE_APP "Compile the example app" ON)
option(BUILD_SHARED_LIBS "Build DeviceMonitor as a shared library" ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)
set(CMAKE_CXX_EXTENSIONS FALSE)

find_package(nlohmann_json CONFIG REQUIRED)
find_package(Threads REQUIRED)

add_library(DeviceMonitor
	src/DeviceMonitor.cpp
	src/Value.cpp
	src/DeviceProvider.cpp
	src/Types.cpp
	src/JSONSerializer.cpp
)

generate_export_header(DeviceMonitor EXPORT_FILE_NAME ${PROJECT_BINARY_DIR}/export/devmon_export.h)

target_include_directories(DeviceMonitor
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/incl>
		$<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/export>
		$<INSTALL_INTERFACE:${CMAKE_INSTALL_PREFIX}/include>
)
target_link_libraries(DeviceMonitor PRIVATE nlohmann_json::nlohmann_json)
target_link_libraries(DeviceMonitor PUBLIC Threads::Threads)

if(COMPILE_TESTS)
	enable_testing()

	find_package(GTest CONFIG REQUIRED)
	add_executable(DeviceMonitor_tests
		src/tests/DeviceMonitorTests.cpp
		src/tests/ValueTests.cpp
		src/tests/DeviceProviderTests.cpp
		src/tests/JSONSerializerTests.cpp
	)

	if(MSVC) # Profiling for code coverage support
		set_target_properties(DeviceMonitor PROPERTIES LINK_FLAGS "/PROFILE")
	endif()
	target_link_libraries(DeviceMonitor_tests PRIVATE DeviceMonitor GTest::gmock_main)

	include(GoogleTest)
	gtest_discover_tests(DeviceMonitor_tests)
endif()

if(COMPILE_EXAMPLE_APP)
	find_package(Argumentum CONFIG REQUIRED)

	add_executable(DeviceMonitor_app
		app/Main.cpp
	)

	target_link_libraries(DeviceMonitor_app PRIVATE Argumentum::argumentum nlohmann_json::nlohmann_json DeviceMonitor)
endif()

install(TARGETS DeviceMonitor DESTINATION ${CMAKE_INSTALL_PREFIX})
install(FILES
	incl/DeviceMonitor/DeviceMonitor.h
	incl/DeviceMonitor/Value.h
	incl/DeviceMonitor/Types.h
	incl/DeviceMonitor/DeviceProvider.h
	${PROJECT_BINARY_DIR}/export/devicemonitor_export.h
	DESTINATION ${CMAKE_INSTALL_PREFIX}/include
)
