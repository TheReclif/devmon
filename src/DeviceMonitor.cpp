#include <DeviceMonitor/DeviceMonitor.h>

void devmon::DeviceMonitor::updateStasuses()
{
	// Uncomment to enable clearing of unavailable/faulty devices.
	//deviceStasuses.clear();

	for (const auto& device : devices)
	try
	{
		deviceStasuses[device.first] = device.second->getProperties();
	}
	catch (...)
	{
		// Do nothing - skip the device (may be faulty).
	}
}

void devmon::DeviceMonitor::threadProc()
{
	std::unique_lock guard(mutex);

	while (!cv.wait_for(guard, delay, [this]() -> bool { return shouldStop; }))
	{
		updateStasuses();
	}
}

devmon::DeviceMonitor::DeviceMonitor(std::chrono::microseconds period)
	: delay(period), shouldStop(false)
{
}

devmon::DeviceMonitor::~DeviceMonitor()
{
	if (isRunning())
	{
		stop();
	}
}

void devmon::DeviceMonitor::start()
{
	if (thread.joinable())
	{
		throw std::runtime_error("Thread joinable - already running");
	}

	updateStasuses();

	shouldStop = false;
	thread = std::thread([this]() { threadProc(); });
}

void devmon::DeviceMonitor::stop()
{
	if (!thread.joinable())
	{
		throw std::runtime_error("Thread not joinable - not running");
	}

	{
		std::lock_guard<std::mutex> guard(mutex);
		shouldStop = true;
		cv.notify_one();
	}
	thread.join();
}

devmon::Dictionary devmon::DeviceMonitor::get_statuses() const
{
	std::lock_guard guard(mutex);

	return devmon::internal::copy(deviceStasuses);
}

bool devmon::DeviceMonitor::isRunning() const
{
	return thread.joinable();
}

void devmon::DeviceMonitor::addDevice(const std::string& deviceId, std::unique_ptr<DeviceProvider>&& provider)
{
	if (!provider)
	{
		throw std::runtime_error("DeviceProvider was null");
	}

	std::lock_guard guard(mutex);

	devices.emplace(deviceId, std::move(provider));
}

void devmon::DeviceMonitor::removeDevice(const std::string& deviceId)
{
	std::lock_guard guard(mutex);

	devices.erase(deviceId);
}

std::size_t devmon::DeviceMonitor::getDeviceCount() const
{
	std::lock_guard guard(mutex);
	return devices.size();
}
