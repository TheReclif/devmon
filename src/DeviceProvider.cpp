#include <DeviceMonitor/DeviceProvider.h>
#include <fstream>
#include <stdexcept>

#include <nlohmann/json.hpp>

devmon::JSONFileDeviceProvider::JSONFileDeviceProvider(std::string_view filename)
	: deviceFilename(filename)
{
}

devmon::Properties devmon::JSONFileDeviceProvider::getProperties()
{
	std::ifstream file(deviceFilename);
	if (!file.is_open())
	{
		throw std::runtime_error("Unable to open a JSON file");
	}

	auto data = nlohmann::json::parse(file);

	if (!data.is_object())
	{
		throw std::runtime_error("JSON data is not an object");
	}

	Properties result;

	for (const auto& property : data.items())
	{
		using value_t = nlohmann::json::value_t;

		switch (property.value().type())
		{
		case value_t::boolean:
			result[property.key()] = std::make_unique<BoolValue>(property.value().get<bool>());
			break;
		case value_t::number_float:
			result[property.key()] = std::make_unique<FloatValue>(property.value().get<float>());
			break;
		case value_t::number_integer:
		case value_t::number_unsigned:
			result[property.key()] = std::make_unique<IntValue>(property.value().get<int>());
			break;
		case value_t::string:
			result[property.key()] = std::make_unique<StringValue>(property.value().get<std::string>());
			break;
		default: // Skip the property.
			break;
		}
	}

	return std::move(result);
}
