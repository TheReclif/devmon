#include <DeviceMonitor/Value.h>
#include <DeviceMonitor/Serializer.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <string_view>

using namespace std::string_literals;
using namespace std::string_view_literals;
using testing::Exactly;

class SerializerMock
	: public devmon::ValueSerializer
{
public:
	MOCK_METHOD(void, serializeBool, (bool val), (override));
	MOCK_METHOD(void, serializeInt, (int val), (override));
	MOCK_METHOD(void, serializeFloat, (float val), (override));
	MOCK_METHOD(void, serializeString, (const std::string& val), (override));
};

TEST(ValueTests, StringValueToStr)
{
	constexpr auto stringVal = "TEST_GOOD"sv;
	std::unique_ptr<devmon::Value> val = std::make_unique<devmon::StringValue>(stringVal);
	EXPECT_EQ(val->toStr(), stringVal);
}

TEST(ValueTests, IntUnsignedValueToStr)
{
	constexpr int intVal = 1337;
	std::unique_ptr<devmon::Value> val = std::make_unique<devmon::IntValue>(intVal);
	const auto valFromStr = std::stoi(val->toStr());
	EXPECT_EQ(valFromStr, intVal);
}

TEST(ValueTests, IntSignedValueToStr)
{
	constexpr int intVal = -1337;
	std::unique_ptr<devmon::Value> val = std::make_unique<devmon::IntValue>(intVal);
	const auto valFromStr = std::stoi(val->toStr());
	EXPECT_EQ(valFromStr, intVal);
}

TEST(ValueTests, FloatValueToStr)
{
	constexpr double floatVal = 0.33;
	std::unique_ptr<devmon::Value> val = std::make_unique<devmon::FloatValue>(floatVal);
	const auto valFromStr = std::stod(val->toStr());
	EXPECT_EQ(valFromStr, floatVal);
}

TEST(ValueTests, BoolValueTrueToStr)
{
	std::unique_ptr<devmon::Value> val = std::make_unique<devmon::BoolValue>(true);
	EXPECT_EQ(val->toStr(), "true");
}

TEST(ValueTests, BoolValueFalseToStr)
{
	std::unique_ptr<devmon::Value> val = std::make_unique<devmon::BoolValue>(false);
	EXPECT_EQ(val->toStr(), "false");
}

TEST(ValueTests, StringValueCopy)
{
	constexpr auto stringVal = "TEST_GOOD"sv;
	std::unique_ptr<devmon::Value> val = std::make_unique<devmon::StringValue>(stringVal);
	const auto copy = val->copy();
	EXPECT_EQ(val->toStr(), copy->toStr());
}

TEST(ValueTests, IntValueCopy)
{
	constexpr int intVal = 1337;
	std::unique_ptr<devmon::Value> val = std::make_unique<devmon::IntValue>(intVal);
	const auto copy = val->copy();
	EXPECT_EQ(val->toStr(), copy->toStr());
}

TEST(ValueTests, FloatValueCopy)
{
	constexpr float floatVal = 0.33f;
	std::unique_ptr<devmon::Value> val = std::make_unique<devmon::FloatValue>(floatVal);
	const auto copy = val->copy();
	EXPECT_EQ(val->toStr(), copy->toStr());
}

TEST(ValueTests, BoolValueCopy)
{
	std::unique_ptr<devmon::Value> val = std::make_unique<devmon::BoolValue>(true);
	const auto copy = val->copy();
	EXPECT_EQ(val->toStr(), copy->toStr());
}

TEST(ValueTests, StringValueGetSet)
{
	devmon::StringValue val("asd");
	EXPECT_EQ(val.toStr(), "asd");
	EXPECT_EQ(val.getValue(), "asd");
	val.setValue("dsa");
	EXPECT_EQ(val.toStr(), "dsa");
	EXPECT_EQ(val.getValue(), "dsa");
}

TEST(ValueTests, IntValueGetSet)
{
	devmon::IntValue val(3);
	EXPECT_EQ(val.getValue(), 3);
	EXPECT_EQ(val.toStr(), "3");
	val.setValue(42);
	EXPECT_EQ(val.getValue(), 42);
	EXPECT_EQ(val.toStr(), "42");
}

TEST(ValueTests, FloatValueGetSet)
{
	devmon::FloatValue val(3.0);
	EXPECT_EQ(val.getValue(), 3.0);
	EXPECT_EQ(std::stod(val.toStr()), 3.0);
	val.setValue(42.0);
	EXPECT_EQ(val.getValue(), 42.0);
	EXPECT_EQ(std::stod(val.toStr()), 42.0);
}

TEST(ValueTests, BoolValueGetSet)
{
	devmon::BoolValue val(true);
	EXPECT_EQ(val.getValue(), true);
	EXPECT_EQ(val.toStr(), "true");
	val.setValue(false);
	EXPECT_EQ(val.getValue(), false);
	EXPECT_EQ(val.toStr(), "false");
}

TEST(ValueTests, StringValueSerialize)
{
	SerializerMock mock;
	devmon::StringValue val("asd");
	EXPECT_CALL(mock, serializeString("asd"s)).Times(Exactly(1));
	val.serialize(mock);
}

TEST(ValueTests, IntValueSerialize)
{
	SerializerMock mock;
	devmon::IntValue val(42);
	EXPECT_CALL(mock, serializeInt(42)).Times(Exactly(1));
	val.serialize(mock);
}

TEST(ValueTests, FloatValueSerialize)
{
	SerializerMock mock;
	devmon::FloatValue val(42.0f);
	EXPECT_CALL(mock, serializeFloat(42.0f)).Times(Exactly(1));
	val.serialize(mock);
}

TEST(ValueTests, BoolValueSerialize)
{
	SerializerMock mock;
	devmon::BoolValue val(true);
	EXPECT_CALL(mock, serializeBool(true)).Times(Exactly(1));
	val.serialize(mock);
}
