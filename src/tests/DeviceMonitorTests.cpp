#include <DeviceMonitor/DeviceMonitor.h>
#include <gtest/gtest.h>

#include <deque>
#include <chrono>

using namespace std::chrono_literals;

class FakeDeviceProvider
	: public devmon::DeviceProvider
{
private:
	unsigned int counter = 0;
	const std::deque<devmon::Properties> properties;
public:
	FakeDeviceProvider() = default;
	FakeDeviceProvider(std::deque<devmon::Properties>&& props)
		: properties(std::move(props))
	{
	}
	virtual ~FakeDeviceProvider() = default;

	virtual devmon::Properties getProperties() override
	{
		const auto val = counter;
		if (counter + 1 < properties.size())
		{
			++counter;
		}
		return devmon::internal::copy(properties[val]);
	}
};

class FaultyDeviceProvider
	: public devmon::DeviceProvider
{
public:
	virtual ~FaultyDeviceProvider() = default;

	virtual devmon::Properties getProperties() override
	{
		throw std::runtime_error("Fake error");
	}
};

class DeviceMonitorTests
	: public testing::Test
{
protected:
	devmon::DeviceMonitor monitor;
protected:
	DeviceMonitorTests()
		: monitor(75ms)
	{}
};

TEST_F(DeviceMonitorTests, DefaultCtorDtor)
{
	EXPECT_NO_THROW({
		devmon::DeviceMonitor monitor(1000ms);
	});
}

TEST_F(DeviceMonitorTests, StartStop)
{
	EXPECT_NO_THROW({
		monitor.start();
		monitor.stop();
	});
}

TEST_F(DeviceMonitorTests, AutoStop)
{
	EXPECT_NO_THROW({
		devmon::DeviceMonitor dev(70ms);
		dev.start();
	});
}

TEST_F(DeviceMonitorTests, DuplicateStart)
{
	EXPECT_NO_THROW(monitor.start());
	EXPECT_THROW(monitor.start(), std::runtime_error);
}

TEST_F(DeviceMonitorTests, DryStop)
{
	EXPECT_THROW(monitor.stop(), std::runtime_error);
}

TEST_F(DeviceMonitorTests, DoubleStop)
{
	EXPECT_NO_THROW(monitor.start());
	EXPECT_NO_THROW(monitor.stop());
	EXPECT_THROW(monitor.stop(), std::runtime_error);
}

TEST_F(DeviceMonitorTests, NoDevicesGetStatuses)
{
	EXPECT_EQ(monitor.get_statuses().size(), 0);
}

TEST_F(DeviceMonitorTests, SingleDeviceGetStatuses)
{
	std::deque<devmon::Properties> props;
	props.emplace_back();
	props[0].emplace(std::make_pair("integer", std::make_unique<devmon::IntValue>(42)));

	monitor.addDevice("dev1", std::make_unique<FakeDeviceProvider>(std::move(props)));
	monitor.start();
	const auto justAfterStartProps = monitor.get_statuses();

	EXPECT_EQ(justAfterStartProps.size(), 1);
	ASSERT_EQ(justAfterStartProps.count("dev1"), 1);
	EXPECT_EQ(justAfterStartProps.at("dev1").size(), 1);
	ASSERT_EQ(justAfterStartProps.at("dev1").count("integer"), 1);
	EXPECT_EQ(justAfterStartProps.at("dev1").at("integer")->toStr(), "42");

	std::this_thread::sleep_for(125ms);

	const auto afterOnePeriodProps = monitor.get_statuses();

	EXPECT_EQ(afterOnePeriodProps.size(), 1);
	ASSERT_EQ(afterOnePeriodProps.count("dev1"), 1);
	EXPECT_EQ(afterOnePeriodProps.at("dev1").size(), 1);
	ASSERT_EQ(afterOnePeriodProps.at("dev1").count("integer"), 1);
	EXPECT_EQ(afterOnePeriodProps.at("dev1").at("integer")->toStr(), "42");
}

TEST_F(DeviceMonitorTests, TwoDevicesGetStatuses)
{
	std::deque<devmon::Properties> props1, props2;
	props1.emplace_back();
	props2.emplace_back();
	props1[0].emplace(std::make_pair("integer", std::make_unique<devmon::IntValue>(42)));
	props2[0].emplace(std::make_pair("str", std::make_unique<devmon::StringValue>("streng")));

	monitor.addDevice("dev1", std::make_unique<FakeDeviceProvider>(std::move(props1)));
	monitor.addDevice("dev2", std::make_unique<FakeDeviceProvider>(std::move(props2)));
	monitor.start();
	const auto justAfterStartProps = monitor.get_statuses();

	EXPECT_EQ(justAfterStartProps.size(), 2);
	ASSERT_EQ(justAfterStartProps.count("dev1"), 1);
	ASSERT_EQ(justAfterStartProps.count("dev2"), 1);
	EXPECT_EQ(justAfterStartProps.at("dev1").size(), 1);
	ASSERT_EQ(justAfterStartProps.at("dev1").count("integer"), 1);
	EXPECT_EQ(justAfterStartProps.at("dev1").at("integer")->toStr(), "42");
	EXPECT_EQ(justAfterStartProps.at("dev2").size(), 1);
	ASSERT_EQ(justAfterStartProps.at("dev2").count("str"), 1);
	EXPECT_EQ(justAfterStartProps.at("dev2").at("str")->toStr(), "streng");

	std::this_thread::sleep_for(125ms);

	const auto afterOnePeriodProps = monitor.get_statuses();

	EXPECT_EQ(justAfterStartProps.size(), 2);
	ASSERT_EQ(justAfterStartProps.count("dev1"), 1);
	ASSERT_EQ(justAfterStartProps.count("dev2"), 1);
	EXPECT_EQ(justAfterStartProps.at("dev1").size(), 1);
	ASSERT_EQ(justAfterStartProps.at("dev1").count("integer"), 1);
	EXPECT_EQ(justAfterStartProps.at("dev1").at("integer")->toStr(), "42");
	EXPECT_EQ(justAfterStartProps.at("dev2").size(), 1);
	ASSERT_EQ(justAfterStartProps.at("dev2").count("str"), 1);
	EXPECT_EQ(justAfterStartProps.at("dev2").at("str")->toStr(), "streng");
}

TEST_F(DeviceMonitorTests, TwoPropSetsGetStasuses)
{
	std::deque<devmon::Properties> props;
	props.emplace_back();
	props[0].emplace(std::make_pair("integer", std::make_unique<devmon::IntValue>(42)));
	props.emplace_back();
	props[1].emplace(std::make_pair("integer", std::make_unique<devmon::IntValue>(34)));

	monitor.addDevice("dev1", std::make_unique<FakeDeviceProvider>(std::move(props)));
	monitor.start();
	const auto justAfterStartProps = monitor.get_statuses();

	EXPECT_EQ(justAfterStartProps.size(), 1);
	ASSERT_EQ(justAfterStartProps.count("dev1"), 1);
	EXPECT_EQ(justAfterStartProps.at("dev1").size(), 1);
	ASSERT_EQ(justAfterStartProps.at("dev1").count("integer"), 1);
	EXPECT_EQ(justAfterStartProps.at("dev1").at("integer")->toStr(), "42");

	std::this_thread::sleep_for(125ms);

	const auto afterOnePeriodProps = monitor.get_statuses();

	EXPECT_EQ(afterOnePeriodProps.size(), 1);
	ASSERT_EQ(afterOnePeriodProps.count("dev1"), 1);
	EXPECT_EQ(afterOnePeriodProps.at("dev1").size(), 1);
	ASSERT_EQ(afterOnePeriodProps.at("dev1").count("integer"), 1);
	EXPECT_EQ(afterOnePeriodProps.at("dev1").at("integer")->toStr(), "34");
}

TEST_F(DeviceMonitorTests, AddDeviceNull)
{
	EXPECT_THROW(monitor.addDevice("dev1", nullptr), std::runtime_error);
}

TEST_F(DeviceMonitorTests, AddDeviceNew)
{
	EXPECT_EQ(monitor.getDeviceCount(), 0);
	monitor.addDevice("dev1", std::make_unique<FakeDeviceProvider>());
	EXPECT_EQ(monitor.getDeviceCount(), 1);
}

TEST_F(DeviceMonitorTests, AddDeviceExisting)
{
	EXPECT_EQ(monitor.getDeviceCount(), 0);
	monitor.addDevice("dev1", std::make_unique<FakeDeviceProvider>());
	EXPECT_EQ(monitor.getDeviceCount(), 1);
	monitor.addDevice("dev1", std::make_unique<FakeDeviceProvider>());
	EXPECT_EQ(monitor.getDeviceCount(), 1);
}

TEST_F(DeviceMonitorTests, RemoveDeviceNonExisting)
{
	EXPECT_EQ(monitor.getDeviceCount(), 0);
	EXPECT_NO_THROW(monitor.removeDevice("dev1"));
	EXPECT_EQ(monitor.getDeviceCount(), 0);
}

TEST_F(DeviceMonitorTests, RemoveDeviceExisting)
{
	EXPECT_EQ(monitor.getDeviceCount(), 0);
	monitor.addDevice("dev1", std::make_unique<FakeDeviceProvider>());
	EXPECT_EQ(monitor.getDeviceCount(), 1);
	monitor.removeDevice("dev1");
	EXPECT_EQ(monitor.getDeviceCount(), 0);
}

TEST_F(DeviceMonitorTests, FaultInDevice)
{
	monitor.addDevice("devFault", std::make_unique<FaultyDeviceProvider>());
	EXPECT_EXIT({
		monitor.start();
		std::this_thread::sleep_for(150ms);
		std::exit(0);
	}, testing::ExitedWithCode(0), "");
}
