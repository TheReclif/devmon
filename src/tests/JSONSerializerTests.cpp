#include <DeviceMonitor/JSONSerializer.h>
#include <DeviceMonitor/Value.h>
#include <gtest/gtest.h>
#include <nlohmann/json.hpp>

using namespace std::string_view_literals;
using nlohmann::json;

class JSONSerializerTests
	: public testing::Test
{
protected:
	devmon::JSONSerializer serializer;
};

TEST_F(JSONSerializerTests, Empty)
{
	EXPECT_EQ(serializer.toStr(), "{}");
}

TEST_F(JSONSerializerTests, OneDevice)
{
	devmon::Dictionary dict;
	dict["dev1"]["integer"] = std::make_unique<devmon::IntValue>(42);
	dict["dev1"]["floating"] = std::make_unique<devmon::FloatValue>(42.0f);
	dict["dev1"]["string"] = std::make_unique<devmon::StringValue>("asd"sv);
	dict["dev1"]["bool"] = std::make_unique<devmon::BoolValue>(true);

	EXPECT_NO_THROW(serializer.serialize(dict));
	json output;
	EXPECT_NO_THROW(output = json::parse(serializer.toStr()));
	json expected = R"(
	{
		"dev1": {
			"integer": 42,
			"floating": 42.0,
			"string": "asd",
			"bool": true
		}
	}
	)"_json;
	EXPECT_EQ(output, expected);
}

TEST_F(JSONSerializerTests, TwoDevices)
{
	devmon::Dictionary dict;
	dict["dev1"]["integer"] = std::make_unique<devmon::IntValue>(42);
	dict["dev1"]["string"] = std::make_unique<devmon::StringValue>("asd"sv);
	dict["dev1"]["bool"] = std::make_unique<devmon::BoolValue>(true);

	dict["dev2"]["voltage"] = std::make_unique<devmon::IntValue>(450);
	dict["dev2"]["id"] = std::make_unique<devmon::StringValue>("id"sv);
	dict["dev2"]["isReady"] = std::make_unique<devmon::BoolValue>(false);

	EXPECT_NO_THROW(serializer.serialize(dict));
	json output;
	EXPECT_NO_THROW(output = json::parse(serializer.toStr()));
	json expected = R"(
	{
		"dev1": {
			"integer": 42,
			"string": "asd",
			"bool": true
		},
		"dev2": {
			"voltage": 450,
			"id": "id",
			"isReady": false
		}
	}
	)"_json;
	EXPECT_EQ(output, expected);
}
