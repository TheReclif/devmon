#include <DeviceMonitor/DeviceProvider.h>
#include <gtest/gtest.h>
#include <fstream>

class JSONFileDeviceProviderTests
	: public testing::Test
{
protected:
	const std::string empty, nonObject, devInt, devBool, devFloat, devString, devMultiple, devUnsupported;
protected:
	JSONFileDeviceProviderTests()
		: empty("empty"), nonObject("nonObject"), devInt("devInt"), devBool("devBool"), devFloat("devFloat"), devString("devString"), devMultiple("devMultiple"), devUnsupported("devUnsupported")
	{
	}

	virtual void SetUp() override
	{
		{
			std::ofstream file(empty);
			ASSERT_TRUE(file.is_open());

			file << "{}\n";

			ASSERT_TRUE(file.good());
		}

		{
			std::ofstream file(nonObject);
			ASSERT_TRUE(file.is_open());

			file << "[1, 2, 3]\n";

			ASSERT_TRUE(file.good());
		}

		{
			std::ofstream file(devInt);
			ASSERT_TRUE(file.is_open());

			file << R"(
			{
				"current": 4
			}
			)";

			ASSERT_TRUE(file.good());
		}

		{
			std::ofstream file(devBool);
			ASSERT_TRUE(file.is_open());

			file << R"(
			{
				"tripped": true
			}
			)";

			ASSERT_TRUE(file.good());
		}

		{
			std::ofstream file(devFloat);
			ASSERT_TRUE(file.is_open());

			file << R"(
			{
				"frequency": 6000.2
			}
			)";

			ASSERT_TRUE(file.good());
		}

		{
			std::ofstream file(devString);
			ASSERT_TRUE(file.is_open());

			file << R"(
			{
				"status": "all green"
			}
			)";

			ASSERT_TRUE(file.good());
		}

		{
			std::ofstream file(devMultiple);
			ASSERT_TRUE(file.is_open());

			file << R"(
			{
				"status": "all green",
				"current": 4,
				"tripped": false,
				"frequency": 6000.2
			}
			)";

			ASSERT_TRUE(file.good());
		}

		{
			std::ofstream file(devUnsupported);
			ASSERT_TRUE(file.is_open());

			file << R"(
			{
				"status": "all green",
				"unsupportedObj": {},
				"unsupportedArr": []
			}
			)";

			ASSERT_TRUE(file.good());
		}
	}
};

TEST_F(JSONFileDeviceProviderTests, NonExistingFile)
{
	std::unique_ptr<devmon::DeviceProvider> deviceProv = std::make_unique<devmon::JSONFileDeviceProvider>("juhkrtgvnirhtkgmrkuthnjkrtbnkub");

	EXPECT_THROW(deviceProv->getProperties(), std::runtime_error);
}

TEST_F(JSONFileDeviceProviderTests, NonObjectFile)
{
	std::unique_ptr<devmon::DeviceProvider> deviceProv = std::make_unique<devmon::JSONFileDeviceProvider>(nonObject);

	EXPECT_THROW(deviceProv->getProperties(), std::runtime_error);
}

TEST_F(JSONFileDeviceProviderTests, EmptyFile)
{
	std::unique_ptr<devmon::DeviceProvider> deviceProv = std::make_unique<devmon::JSONFileDeviceProvider>(empty);

	EXPECT_EQ(deviceProv->getProperties().size(), 0);
}

TEST_F(JSONFileDeviceProviderTests, FileWithIntProperty)
{
	std::unique_ptr<devmon::DeviceProvider> deviceProv = std::make_unique<devmon::JSONFileDeviceProvider>(devInt);
	const auto props = deviceProv->getProperties();

	EXPECT_EQ(props.size(), 1);
	ASSERT_EQ(props.count("current"), 1);

	EXPECT_EQ(props.at("current")->toStr(), "4");
}

TEST_F(JSONFileDeviceProviderTests, FileWithBoolProperty)
{
	std::unique_ptr<devmon::DeviceProvider> deviceProv = std::make_unique<devmon::JSONFileDeviceProvider>(devBool);
	const auto props = deviceProv->getProperties();

	EXPECT_EQ(props.size(), 1);
	ASSERT_EQ(props.count("tripped"), 1);

	EXPECT_EQ(props.at("tripped")->toStr(), "true");
}

TEST_F(JSONFileDeviceProviderTests, FileWithFloatProperty)
{
	std::unique_ptr<devmon::DeviceProvider> deviceProv = std::make_unique<devmon::JSONFileDeviceProvider>(devFloat);
	const auto props = deviceProv->getProperties();

	EXPECT_EQ(props.size(), 1);
	ASSERT_EQ(props.count("frequency"), 1);

	EXPECT_NEAR(std::stod(props.at("frequency")->toStr()), 6000.2, 0.1);
}

TEST_F(JSONFileDeviceProviderTests, FileWithStringProperty)
{
	std::unique_ptr<devmon::DeviceProvider> deviceProv = std::make_unique<devmon::JSONFileDeviceProvider>(devString);
	const auto props = deviceProv->getProperties();

	EXPECT_EQ(props.size(), 1);
	ASSERT_EQ(props.count("status"), 1);

	EXPECT_EQ(props.at("status")->toStr(), "all green");
}

TEST_F(JSONFileDeviceProviderTests, FileWithMultipleProperties)
{
	std::unique_ptr<devmon::DeviceProvider> deviceProv = std::make_unique<devmon::JSONFileDeviceProvider>(devMultiple);
	const auto props = deviceProv->getProperties();

	EXPECT_EQ(props.size(), 4);

	EXPECT_EQ(props.at("status")->toStr(), "all green");
	EXPECT_EQ(props.at("tripped")->toStr(), "false");
	EXPECT_EQ(props.at("current")->toStr(), "4");
	EXPECT_NEAR(std::stod(props.at("frequency")->toStr()), 6000.2, 0.1);
}

TEST_F(JSONFileDeviceProviderTests, UnsupportedPropertyTypes)
{
	std::unique_ptr<devmon::DeviceProvider> deviceProv = std::make_unique<devmon::JSONFileDeviceProvider>(devUnsupported);
	const auto props = deviceProv->getProperties();

	EXPECT_EQ(props.size(), 1);

	EXPECT_EQ(props.at("status")->toStr(), "all green");
}

TEST_F(JSONFileDeviceProviderTests, MoveCtor)
{
	devmon::JSONFileDeviceProvider original("");
	EXPECT_NO_THROW({ devmon::JSONFileDeviceProvider movedTo(std::move(original)); });
}

TEST_F(JSONFileDeviceProviderTests, MoveAssignment)
{
	devmon::JSONFileDeviceProvider original(""), movedTo("");
	EXPECT_NO_THROW({ movedTo = std::move(original); });
}
