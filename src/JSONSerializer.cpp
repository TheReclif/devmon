#include <DeviceMonitor/JSONSerializer.h>

#include <nlohmann/json.hpp>

using nlohmann::json;

class JSONValueSerializer
	: public devmon::ValueSerializer
{
public:
	json object;
public:
	virtual ~JSONValueSerializer() = default;

	virtual void serializeInt(int val) override
	{
		object = val;
	}
	virtual void serializeFloat(float val) override
	{
		object = val;
	}
	virtual void serializeString(const std::string& val) override
	{
		object = val;
	}
	virtual void serializeBool(bool val) override
	{
		object = val;
	}
};

class devmon::JSONSerializer::impl
{
public:
	json object = json::object();
};

devmon::JSONSerializer::JSONSerializer()
	: implPtr(std::make_unique<impl>())
{
}

devmon::JSONSerializer::~JSONSerializer()
{
}

std::string devmon::JSONSerializer::toStr() const
{
	return implPtr->object.dump(4);
}

void devmon::JSONSerializer::serialize(const Dictionary& dict)
{
	json result = json::object();

	for (const auto& x : dict)
	{
		JSONSerializer propertiesSerializer;
		propertiesSerializer.serialize(x.second);
		result[x.first] = std::move(propertiesSerializer.implPtr->object);
	}

	implPtr->object = std::move(result);
}

void devmon::JSONSerializer::serialize(const Properties& props)
{
	json result = json::object();

	for (const auto& x : props)
	{
		JSONValueSerializer valueSerializer;
		x.second->serialize(valueSerializer);
		result[x.first] = std::move(valueSerializer.object);
	}

	implPtr->object = std::move(result);
}
