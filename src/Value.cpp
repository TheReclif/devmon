#include <DeviceMonitor/Value.h>
#include <DeviceMonitor/Serializer.h>

#include <charconv>

devmon::StringValue::StringValue(std::string_view newValue)
	: value(newValue)
{
}

void devmon::StringValue::setValue(std::string_view newValue)
{
	value = newValue;
}

std::string devmon::StringValue::getValue() const
{
	return value;
}

std::string devmon::StringValue::toStr() const
{
	return value;
}

std::unique_ptr<devmon::Value> devmon::StringValue::copy() const
{
	return std::make_unique<StringValue>(value);
}

void devmon::StringValue::serialize(ValueSerializer& serializer) const
{
	serializer.serializeString(value);
}

devmon::IntValue::IntValue(int newValue)
	: value(newValue)
{
}

void devmon::IntValue::setValue(int newValue)
{
	value = newValue;
}

int devmon::IntValue::getValue() const
{
	return value;
}

std::string devmon::IntValue::toStr() const
{
	return std::to_string(value);
}

std::unique_ptr<devmon::Value> devmon::IntValue::copy() const
{
	return std::make_unique<IntValue>(value);
}

void devmon::IntValue::serialize(ValueSerializer& serializer) const
{
	serializer.serializeInt(value);
}

devmon::FloatValue::FloatValue(double newValue)
	: value(newValue)
{
}

void devmon::FloatValue::setValue(double newValue)
{
	value = newValue;
}

double devmon::FloatValue::getValue() const
{
	return value;
}

std::string devmon::FloatValue::toStr() const
{
	return std::to_string(value);
}

std::unique_ptr<devmon::Value> devmon::FloatValue::copy() const
{
	return std::make_unique<FloatValue>(value);
}

void devmon::FloatValue::serialize(ValueSerializer& serializer) const
{
	serializer.serializeFloat(value);
}

devmon::BoolValue::BoolValue(bool newValue)
	: value(newValue)
{
}

void devmon::BoolValue::setValue(bool newValue)
{
	value = newValue;
}

bool devmon::BoolValue::getValue() const
{
	return value;
}

std::string devmon::BoolValue::toStr() const
{
	return value ? "true" : "false";
}

std::unique_ptr<devmon::Value> devmon::BoolValue::copy() const
{
	return std::make_unique<BoolValue>(value);
}

void devmon::BoolValue::serialize(ValueSerializer& serializer) const
{
	serializer.serializeBool(value);
}
