#include <DeviceMonitor/Types.h>
#include <DeviceMonitor/Value.h>

devmon::Properties devmon::internal::copy(const Properties& props)
{
	devmon::Properties result;

	for (const auto& x : props)
	{
		result.emplace(x.first, x.second->copy());
	}

	return std::move(result);
}

devmon::Dictionary devmon::internal::copy(const Dictionary& dict)
{
	devmon::Dictionary result;

	for (const auto& x : dict)
	{
		result.emplace(x.first, copy(x.second));
	}

	return std::move(result);
}
